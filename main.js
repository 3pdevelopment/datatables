$(document).ready(function () {
    $('#my_table').dataTable({
        "bJQueryUI": true,
//        "bStateSave": true
    }).yadcf([
        {column_number: 0, filter_type: "multi_select", select_type: 'select2'},
        {column_number: 1, filter_type: "multi_select", select_type: 'select2'},
        {column_number: 2, select_type: 'select2'},
        {column_number: 3, select_type: 'select2'},
        {column_number: 4, select_type: 'select2'},
        {column_number: 5, filter_type: "range_number_slider"},
        {column_number: 6, select_type: 'select2'},
        {column_number: 7, filter_type: "range_number_slider"},
        {column_number: 8, select_type: 'select2', text_data_delimiter: ","},
        {column_number: 9, select_type: 'select2'}
    ]);
});