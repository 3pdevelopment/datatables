<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="images/favicon.png">

        <link rel="stylesheet" type="text/css" href="http://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/css/jquery.dataTables.css">
        <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/themes/base/jquery-ui.css" rel="stylesheet" type="text/css" />
        <link href="resources/jquery.dataTables.yadcf.css" rel="stylesheet" type="text/css" />
        
        <link href="resources/chosen.min.css" rel="stylesheet" type="text/css" />
        <link href="resources/select2.css" rel="stylesheet" type="text/css" />
        <link href="resources/jquery.dataTables_themeroller.css" rel="stylesheet" type="text/css" />
        <link href="main.css" rel="stylesheet" type="text/css" />
        
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>
        <script type="text/javascript" charset="utf8" src="http://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/jquery.dataTables.min.js"></script>
        <script src="resources/chosen.jquery.min.js"></script>
        <script src="resources/select2.js"></script>
        <script src="resources/jquery.dataTables.yadcf.js"></script>
        
        <script type="text/javascript" src="main.js"></script>
    </head>
    <body>
    <?php include "config.php"; ?>

    <?php
        $sql = "SELECT * FROM my_table ORDER BY id DESC LIMIT 500";
        $result = $conn->query($sql);
    ?>
    
    <table id="my_table">
        <thead>
            <tr>
                <th>Sponsor name</th>
                <th>City</th>
                <th>State</th>
                <th>ZIP</th>
                <th>EIN</th>
                <th>Participants</th>
                <th>Parent industry</th>
                <th>Total broker revenue</th>
                <th>Tag broker</th>
                <th>Tag carrier</th>
            </tr>
        </thead>
        <tbody>
            <?php if ($result->num_rows > 0) { ?>
                <?php while($row = $result->fetch_assoc()) { ?>
            <tr>
                    <td><?php echo $row["SPONSOR_NAME"] ?></td>
                    <td><?php echo $row["CITY"] ?></td>
                    <td><?php echo $row["STATE"] ?></td>
                    <td><?php echo $row["ZIP"] ?></td>
                    <td><?php echo $row["EIN"] ?></td>
                    <td><?php echo $row["PARTICIPANTS"] ?></td>
                    <td><?php echo $row["PARENT_INDUSTRY"] ?></td>
                    <td><?php echo $row["TOTAL_BROKER_REVENUE"] ?></td>
                    <td><?php echo $row["TAG_BROKER"] ?></td>
                    <td><?php echo $row["TOTAL_PREMIUM"] ?></td>
            </tr>
                <?php } ?>
            <?php } else { ?>
                0 results
            <?php } ?>
        </tbody>
    </table>
    
<?php $conn->close(); ?>
    
</body>
</html>

